// Code generated by MockGen. DO NOT EDIT.
// Source: ./guid/export_test.go

// Package mocks is a generated GoMock package.
package mocks

import (
	io "io"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockMocker is a mock of Mocker interface.
type MockMocker struct {
	ctrl     *gomock.Controller
	recorder *MockMockerMockRecorder
}

// MockMockerMockRecorder is the mock recorder for MockMocker.
type MockMockerMockRecorder struct {
	mock *MockMocker
}

// NewMockMocker creates a new mock instance.
func NewMockMocker(ctrl *gomock.Controller) *MockMocker {
	mock := &MockMocker{ctrl: ctrl}
	mock.recorder = &MockMockerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockMocker) EXPECT() *MockMockerMockRecorder {
	return m.recorder
}

// ReadFull mocks base method.
func (m *MockMocker) ReadFull(r io.Reader, buf []byte) (int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReadFull", r, buf)
	ret0, _ := ret[0].(int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ReadFull indicates an expected call of ReadFull.
func (mr *MockMockerMockRecorder) ReadFull(r, buf interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReadFull", reflect.TypeOf((*MockMocker)(nil).ReadFull), r, buf)
}

// DecodeString mocks base method.
func (m *MockMocker) DecodeString(s string) ([]byte, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DecodeString", s)
	ret0, _ := ret[0].([]byte)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// DecodeString indicates an expected call of DecodeString.
func (mr *MockMockerMockRecorder) DecodeString(s interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DecodeString", reflect.TypeOf((*MockMocker)(nil).DecodeString), s)
}
