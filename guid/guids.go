// Copyright 2021 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package guid

import (
	"gitlab.com/tymonx/go-error/rterror"
)

// GUIDs defines a list of global unique IDs.
type GUIDs []GUID

// FromStrings creates list of global unique IDs from list of strings.
func FromStrings(strs ...string) (ids GUIDs, err error) {
	ids = make(GUIDs, len(strs))

	for i, str := range strs {
		if ids[i], err = FromString(str); err != nil {
			return GUIDs{}, rterror.New("cannot create ID from string", str, "at", i).Wrap(err)
		}
	}

	return ids, nil
}

// ToStrings returns list of global unique IDs as list of strings.
func (g GUIDs) ToStrings() []string {
	ids := make([]string, len(g))

	for i, id := range g {
		ids[i] = id.String()
	}

	return ids
}
