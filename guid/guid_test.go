// Copyright 2021 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package guid_test

import (
	"encoding/json"
	"regexp"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/go-id/guid"
	"gitlab.com/tymonx/go-id/guid/mocks"
)

func TestGUIDNew(test *testing.T) {
	want := 1024
	guids := make(map[guid.GUID]bool)

	uuid4Regexp := regexp.MustCompile(guid.Schema)

	for i := 0; i < want; i++ {
		id := guid.New()

		assert.True(test, uuid4Regexp.MatchString(id.String()))

		guids[id] = true
	}

	assert.Len(test, guids, want)
}

func TestGUIDNewReaderError(test *testing.T) {
	controller := gomock.NewController(test)
	defer controller.Finish()

	mock := mocks.NewMockMocker(controller)
	defer guid.Mock(mock)()

	mock.EXPECT().ReadFull(gomock.Any(), gomock.Any()).AnyTimes().Return(0, rterror.New("error"))

	want := 1024
	guids := make(map[guid.GUID]bool)

	uuid4Regexp := regexp.MustCompile(guid.Schema)

	for i := 0; i < want; i++ {
		id := guid.New()

		assert.True(test, uuid4Regexp.MatchString(id.String()))

		guids[id] = true
	}

	assert.Len(test, guids, want)
}

func TestGUIDLength(test *testing.T) {
	g := guid.New()

	assert.Equal(test, guid.Length, g.Length())
}

func TestGUIDHex(test *testing.T) {
	g := guid.New()

	assert.NotEmpty(test, g.Hex())
}

func TestGUIDString(test *testing.T) {
	g := guid.New()

	assert.NotEmpty(test, g.String())
}

func TestGUIDStringEmpty(test *testing.T) {
	var g guid.GUID

	assert.Empty(test, g.String())
}

func TestGUIDValid(test *testing.T) {
	g := guid.New()

	assert.True(test, g.Valid())
}

func TestGUIDNotValid(test *testing.T) {
	var g guid.GUID

	assert.False(test, g.Valid())
}

func TestGUIDIsValid(test *testing.T) {
	g := guid.New()

	assert.True(test, g.IsValid())
}

func TestGUIDNotIsValid(test *testing.T) {
	var g guid.GUID

	assert.False(test, g.IsValid())
}

func TestGUIDIsSet(test *testing.T) {
	g := guid.New()

	assert.True(test, g.IsSet())
	assert.False(test, g.IsNotSet())
}

func TestGUIDNotIsSet(test *testing.T) {
	var g guid.GUID

	assert.False(test, g.IsSet())
	assert.True(test, g.IsNotSet())
}

func TestGUIDBytes(test *testing.T) {
	g := guid.New()

	assert.Equal(test, guid.Length, len(g.Bytes()))
	assert.NotZero(test, g)
}

func TestGUIDUnmarshalJSON(test *testing.T) {
	var g guid.GUID

	assert.NoError(test, json.Unmarshal([]byte(` " 327fD14c-d4bc-47d2-9d6d-C8109d7a6219 " `), &g))

	assert.True(test, g.Valid())
	assert.Equal(test, "327fd14c-d4bc-47d2-9d6d-c8109d7a6219", g.String())
}

func TestGUIDUnmarshalJSONNotMatch(test *testing.T) {
	var g guid.GUID

	assert.Error(test, json.Unmarshal([]byte(`"327fD14c-d4bc-47dX-9d6d-C8109d7a6219"`), &g))

	assert.False(test, g.Valid())
	assert.Empty(test, g.String())
}

func TestGUIDUnmarshalJSONDecodeStringError(test *testing.T) {
	controller := gomock.NewController(test)
	defer controller.Finish()

	mock := mocks.NewMockMocker(controller)
	defer guid.Mock(mock)()

	want := "327fD14c-d4bc-47d1-9d6d-C8109d7a6219"

	mock.EXPECT().DecodeString(strings.ReplaceAll(want, "-", "")).Return(nil, rterror.New("error"))

	var g guid.GUID

	assert.Error(test, json.Unmarshal([]byte(`"`+want+`"`), &g))

	assert.False(test, g.Valid())
	assert.Empty(test, g.String())
}

func TestGUIDUnmarshalJSONEmpty(test *testing.T) {
	g := guid.New()

	assert.NoError(test, json.Unmarshal([]byte(`""`), &g))

	assert.False(test, g.Valid())
	assert.Empty(test, g.String())
}

func TestGUIDUnmarshalJSONNull(test *testing.T) {
	g := guid.New()

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &g))

	assert.True(test, g.Valid())
	assert.NotEmpty(test, g.String())
}

func TestGUIDUnmarshalJSONInvalid(test *testing.T) {
	var g guid.GUID

	assert.Error(test, json.Unmarshal([]byte(`5`), &g))

	assert.False(test, g.Valid())
	assert.Empty(test, g.String())
}

func TestGUIDMarshalJSON(test *testing.T) {
	g := guid.New()

	data, err := json.Marshal(&g)

	assert.NoError(test, err)
	assert.NotEmpty(test, data)
	assert.True(test, regexp.MustCompile(guid.Schema).MatchString(strings.Trim(string(data), `"`)))
}

func TestGUIDMarshalJSONEmpty(test *testing.T) {
	var g guid.GUID

	data, err := json.Marshal(&g)

	assert.NoError(test, err)
	assert.Equal(test, []byte(`""`), data)
}

func TestGUIDMarshalText(test *testing.T) {
	g := guid.New()

	data, err := g.MarshalText()

	assert.NoError(test, err)
	assert.NotEmpty(test, data)
	assert.True(test, regexp.MustCompile(guid.Schema).MatchString(string(data)))
}

func TestGUIDFromBytesOK(test *testing.T) {
	bytes := [guid.Length]byte{0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}

	g, err := guid.FromBytes(bytes[:])

	assert.NoError(test, err)
	assert.Equal(test, "00112233-4455-6677-8899-aabbccddeeff", g.String())
}

func TestGUIDFromBytesErrorNil(test *testing.T) {
	g, err := guid.FromBytes(nil)

	assert.Error(test, err)
	assert.Empty(test, g.String())
}

func TestGUIDFromBytesErrorZero(test *testing.T) {
	g, err := guid.FromBytes([]byte{})

	assert.Error(test, err)
	assert.Empty(test, g.String())
}

func TestGUIDFromBytesErrorLess(test *testing.T) {
	bytes := [guid.Length - 1]byte{0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE}

	g, err := guid.FromBytes(bytes[:])

	assert.Error(test, err)
	assert.Empty(test, g.String())
}

func TestGUIDFromBytesErrorLarger(test *testing.T) {
	bytes := [guid.Length + 1]byte{0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11}

	g, err := guid.FromBytes(bytes[:])

	assert.Error(test, err)
	assert.Empty(test, g.String())
}

func TestGUIDIsNilTrue(test *testing.T) {
	assert.True(test, guid.GUID{}.IsNil())
}

func TestGUIDIsNilFalse(test *testing.T) {
	assert.False(test, guid.New().IsNil())
}

func TestGUIDIsEmptyTrue(test *testing.T) {
	assert.True(test, guid.GUID{}.IsEmpty())
}

func TestGUIDIsEmptyFalse(test *testing.T) {
	assert.False(test, guid.New().IsEmpty())
}

func TestGUIDFromStringOK(test *testing.T) {
	want := "49e66f08-613d-49df-bf4b-0d4d85790fe2"

	g, err := guid.FromString(want)

	assert.NoError(test, err)
	assert.Equal(test, want, g.String())
}

func TestGUIDFromStringError(test *testing.T) {
	g, err := guid.FromString("invalid")

	assert.Error(test, err)
	assert.Empty(test, g.String())
}
