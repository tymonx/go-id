// Copyright 2021 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package guid

import (
	"encoding/hex"
	"io"
)

type Mocker interface {
	ReadFull(r io.Reader, buf []byte) (n int, err error)

	DecodeString(s string) ([]byte, error)
}

func Mock(mock Mocker) func() { // setup
	ioReadFull = mock.ReadFull
	hexDecodeString = mock.DecodeString

	return func() { // teardown
		ioReadFull = io.ReadFull
		hexDecodeString = hex.DecodeString
	}
}
