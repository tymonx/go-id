// Copyright 2021 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package guid_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/go-id/guid"
)

func TestGUIDsFromStringsEmpty(test *testing.T) {
	ids, err := guid.FromStrings()

	assert.NoError(test, err)
	assert.Empty(test, ids)
}

func TestGUIDsFromStringsOne(test *testing.T) {
	want := "956c2502-bf46-4cb3-93a9-caab3644c11c"

	ids, err := guid.FromStrings(want)

	assert.NoError(test, err)
	assert.Len(test, ids, 1)
	assert.Equal(test, want, ids[0].String())
}

func TestGUIDsFromStringsMulti(test *testing.T) {
	wants := []string{
		"c61098e3-d86c-461e-8e6e-ef4850161a35",
		"b0164ecf-1927-4279-8346-71785af75f3b",
		"fa3a91ba-7662-4db5-8b13-222097aafff9",
	}

	ids, err := guid.FromStrings(wants...)

	assert.NoError(test, err)
	assert.Len(test, ids, len(wants))

	for i, want := range wants {
		assert.Equal(test, want, ids[i].String())
	}
}

func TestGUIDsFromStringsError(test *testing.T) {
	ids, err := guid.FromStrings("28388a08-a4dd-412f-bfe6-a0ae868b00ec", "invalid")

	assert.Error(test, err)
	assert.Empty(test, ids)
}

func TestGUIDsToStringsError(test *testing.T) {
	assert.Equal(test, []string{
		"12345678-0000-0000-0000-000000000000",
		"12345678-9abc-def0-0000-000000000000",
	}, guid.GUIDs{
		{0x12, 0x34, 0x56, 0x78},
		{0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0},
	}.ToStrings())
}
