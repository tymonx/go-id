// Copyright 2021 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package guid

import (
	crand "crypto/rand"
	"encoding/hex"
	"io"
	"math/rand"
	"regexp"
	"strings"
	"sync"
	"time"

	"gitlab.com/tymonx/go-error/rterror"
)

// These constants define values for global unique ID.
const (
	Length = 16
	Schema = `^[[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12}$`
)

// GUID defines global unique ID type.
type GUID [Length]byte

var (
	ioReadFull      = io.ReadFull                // nolint: gochecknoglobals
	hexDecodeString = hex.DecodeString           // nolint: gochecknoglobals
	gOnce           sync.Once                    // nolint: gochecknoglobals
	gMutex          sync.Mutex                   // nolint: gochecknoglobals
	gRand           *rand.Rand                   // nolint: gochecknoglobals
	gRegexp         = regexp.MustCompile(Schema) // nolint: gochecknoglobals
	gNil            GUID                         // nolint: gochecknoglobals
)

// New creates a new global unique ID object.
func New() (g GUID) {
	if _, err := ioReadFull(crand.Reader, g[:]); err != nil {
		gOnce.Do(func() {
			gRand = rand.New(rand.NewSource(time.Now().UnixNano())) //nolint: gosec // failback for crypto/rand
		})

		func() {
			gMutex.Lock()
			defer gMutex.Unlock()

			for i := range g {
				g[i] = byte(gRand.Int())
			}
		}()
	}

	// UUID4 format:
	g[6] = 0x40 | (g[6] & 0x0F)
	g[8] = 0x80 | (g[8] & 0x3F)

	return g
}

// FromBytes creates a new global unique ID from bytes.
//
// It returns an error, if provided length bytes is different from 16 bytes.
func FromBytes(bytes []byte) (id GUID, err error) {
	if length := len(bytes); length != Length {
		return id, rterror.New("invalid number of bytes", length, "must be", Length)
	}

	copy(id[:], bytes)

	return id, nil
}

// FromString creates a new global unique ID from string.
//
// It returns an error, if provided string is ill-formatted.
func FromString(str string) (id GUID, err error) {
	if err = id.UnmarshalText([]byte(str)); err != nil {
		return id, rterror.New("cannot create ID from string", str).Wrap(err)
	}

	return id, nil
}

// String returns global unique ID as a string in the UUID format: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.
//
// If global unique ID was not initialized, it returns empty string.
// It never returns 00000000-0000-0000-0000-000000000000. This will allow to detect Nil UUID.
func (g GUID) String() string {
	if !g.Valid() {
		return ""
	}

	str := g.Hex()

	return str[0:8] + "-" + str[8:12] + "-" + str[12:16] + "-" + str[16:20] + "-" + str[20:32]
}

// Hex returns global unique ID as a string in the hex format: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
func (g GUID) Hex() string {
	return hex.EncodeToString(g[:])
}

// IsNil returns true if global unique ID is Nil UUID. Otherwise, it returns false.
func (g GUID) IsNil() bool {
	return g == gNil
}

// IsEmpty returns true if global unique ID is Nil UUID. Otherwise, it returns false.
func (g GUID) IsEmpty() bool {
	return g == gNil
}

// IsValid returns true if global unique ID was initialized. Otherwise, it returns false.
func (g GUID) IsValid() bool {
	return g != gNil
}

// IsSet returns true if global unique ID was set. Otherwise, it returns false.
func (g GUID) IsSet() bool {
	return g != gNil
}

// IsNotSet returns true if global unique ID was not set. Otherwise, it returns false.
func (g GUID) IsNotSet() bool {
	return g == gNil
}

// Valid returns true if global unique ID was initialized. Otherwise, it returns false.
func (g GUID) Valid() bool {
	return g != gNil
}

// Length returns global unique ID length.
func (g GUID) Length() int {
	return len(g)
}

// Bytes returns global unique ID as bytes.
func (g GUID) Bytes() []byte {
	return g[:]
}

// UnmarshalText decodes text to global unique ID.
func (g *GUID) UnmarshalText(text []byte) (err error) {
	str := strings.TrimSpace(string(text))

	if str == "" {
		*g = GUID{}
		return nil
	}

	if !gRegexp.MatchString(str) {
		return rterror.New("ID", string(text), "must be in xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx UUID format")
	}

	str = strings.ReplaceAll(str, "-", "")

	var data []byte

	if data, err = hexDecodeString(str); err != nil {
		return rterror.New("cannot decode string to bytes", str).Wrap(err)
	}

	copy(g[:], data)

	return nil
}

// MarshalText encodes global unique ID to text.
func (g GUID) MarshalText() ([]byte, error) {
	return []byte(g.String()), nil
}
