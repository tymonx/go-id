# Go ID

The Go ID library implements ID generators.

[[_TOC_]]

## Features

* Global unique ID generator in `UUID4` format

## Usage

Import `guid` package:

```go
import "gitlab.com/tymonx/go-id/guid"
```

### Generate ID

```go
id := guid.New()

fmt.Println(id)
```

Output:

```plaintext
85d533a2-1c9a-46d5-a7ed-d1de01d86798
```
